# README #

### Introduction ###

Google Test is a unit testing framework for the C++ programming language, based on the xUnit architecture. 
It can be compiled for a variety of platforms, allowing unit-testing of C as well as C++ with minimal modification (“Google Test”, n.d.). 
The unit testing framework would be a good tool for the Test-Driven Development which makes it feasible for programmers to make better design code, 
faster incremental upgrades, refactoring with confidence and preventing recurring bugs. Also, Other people can work with the produced code more easily.   

### Functionalities ###

Google test provides functionalities to fulfill the following requirements for Unit testing framework (“Test-driven”, n.d.).
*	Writing of unit tests should be simple and obvious.
*	Framework should allow advanced users’ nontrivial tests.
*	Should be able to have many small test cases and to group them into test suites.
*	Short compilation time.
*	Being able to see the test's progress.
*	Execution of individual tests should be independent on other tests. For example, termination of one test shouldn't lead to skipping of other tests.
*	Simple tests shouldn't require an external library.

### Mocking (Google Mock) ###
Google test supports mocking object which can simulate behavior of complex, real objects. If an object has any of the following characteristics, it may be useful to use the mocking (“Unit Testing”, n.d.).
*	Non-deterministic
*	Difficult to create or reproduce a status and slow
*	Does not exist yet

### The mocking framework follows standard workflow of mocking. (“Test-driven”, n.d.). ###
* Creates a mock object for given class. 
* Users need to set expectation of the mock object with many supporting functions
* Make and run test cases using mock objects.
* On destruction of mock object, Google mock checks expectations against actual results, and if they weren't met, then it will report error.

### Installation on Visual studio ###
* Execute Tools > NuGet Package Manager > Manager NuGet Packages for Solution
* Select Google test and Google Mock and install

### Major Google Test Macros ###
* TEST() 
	This macro is necessary to tell Google Test that you are writing a test
* EXPECT/ASSERT : 
	Checks whether the statement in parentheses is true or not
* EXPECT 
	does not stop execution. If you want to stop, use ASSERT.
	Message 
	<< operator to give a human-readable message when the case is failed.

* Initialization testing::InitGoogleTest(&ac, av);
	Run tests RUN_ALL_TESTS() executes all test cases

### Making Mock Class ###
Derive a Mock class from Interface class.
1. Take a virtual function. Count how many arguments it has. 
2. Inside the child class, write MOCK_METHODn(), where n is the number of the arguments.
3. Cut-and-paste the function name as the first argument to the macro, and leave what's left as the second argument.
4. Repeat until all virtual functions you want to mock are done.

### Setting expectation and Running ###
The mockDB object's method will be called one time during the test, and it will return true as a result.
You can define more expectation by following general syntax. Through the Test Explorer, all tests or some part of tests can be executed.

### References ###
Google Test. (n.d.). Retrieved from https://github.com/google/googletest
Unit Testing with JUnit, TestNG and Mockito. (n.d.). Retrieved from https://zeroturnaround.com/rebellabs/draft-keeping-your-code-safe-with-junit-testing-and-mockito/8/
Test-driven development and unit testing with examples in C++. (n.d.). Retrieved from http://alexott.net/en/cpp/CppTestingIntro.html

