#pragma once
#include <gtest/gtest.h>
#include "MockDB.h"

class Sprint
{
	int			_numOfMembers;
	int			_duration;
	int			_numOfUserStories;
	IDataBase*	_database;

public :
	Sprint() : _numOfMembers(0), _duration(0), _numOfUserStories(0), _database(nullptr){};
	~Sprint() {};

	/* For members */
	void PutMembers(int num) { _numOfMembers = num; };
	int	 GetNumOfMembers() { return _numOfMembers; };

	/* For duration */
	void SetDuration(int duration) { _duration = duration; };

	/* For user stories */
	int	GetDuration() { return _duration; };
	void AddUserStories(int num) { _numOfUserStories += num; };
	int GetUserStories() { return _numOfUserStories; };
	void DeleteUserStories(int num) { _numOfUserStories -= num; };

	/* For DB*/
	void SetDB(IDataBase* database) { _database = database; };

	/* Write data into DB */
	bool UpdateDB()
	{
		return _database->UpdateSolvedUserStories(_numOfUserStories);
	};

};

/**
 * Whenever you��re testing stuff, you are probably testing a class or set of classes. 
 * Setting up and destroying them on every test might be painful and this is what test fixtures are for.
 * A test fixture is a class that inherits from ::testing::Test 
 */
struct RADTest : testing::Test
{
	Sprint*	 sprint;

	RADTest()
	{
		sprint = new Sprint();
	}

	virtual ~RADTest()
	{
		delete sprint;
	}
};