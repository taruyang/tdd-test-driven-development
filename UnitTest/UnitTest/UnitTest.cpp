/* To install GTest on your visual studio, let you find "Google Test" on NuGet Package manager. */
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "TestFixture.h"
#include "MockDB.h"

int add(int l, int r) {
	return l + r;
}

float add(float l, float r) {
	return l + r;
}

int sub(int l, int r) {
	return l - r;
}

/**
 * TEST() macro is necessary to tell Google Test that you are writing a test. 
 * A single test can contain more than once statement.
 */
TEST(Arithmetic1, CanAddTwoNumbers)
{
	EXPECT_TRUE(add(2, 2) == 3) << "Two plus two must be four";
	EXPECT_FALSE(add(2, 3) == 4);

	/**
	 * EXPECT_EQ() tests that the second argument is the same as the first.
	 * << operator to give a human-readable message to 
	 * whoever is running the test in case it fails.
	 */
	EXPECT_EQ(4, add(2, 2)) << "Two plus two must equal four";

	/**
	 * EXPECT_ mecro does not stop execution. If you want to stop execution if it fails
	 * use ASSERT_ mecro
	 */
	ASSERT_EQ(7, add(2, 5));

	/**
	 * Some variations
	 */
	EXPECT_LT(add(7, 2), 10);
	EXPECT_FLOAT_EQ(add(1.1f, 2.2f), 3.3f);
}

TEST(Arithmetic, CanSubTwoNumbers)
{
	EXPECT_EQ(2, sub(4, 2)) << " Four minus two must equal two";
}

int main(int ac, char* av[])
{
	/* The call to InitGoogleTest() parses command-line arguments: 
	 * you can specify your own arguments when run the executable file by passing some arguments.
	 */
	testing::InitGoogleTest(&ac, av);

	return RUN_ALL_TESTS();
}

/**
* TEST_F is a macro to test Fixture
*/
TEST_F(RADTest, CanAddUserStories)
{
	sprint->AddUserStories(5);
	EXPECT_EQ(5, sprint->GetUserStories());
	sprint->AddUserStories(2);
	EXPECT_EQ(7, sprint->GetUserStories());
}

TEST_F(RADTest, CanDeleteUserStories)
{
	sprint->AddUserStories(5);
	EXPECT_EQ(5, sprint->GetUserStories());
	sprint->DeleteUserStories(2);
	EXPECT_EQ(3, sprint->GetUserStories());
}

TEST_F(RADTest, CanSaveDataIntoDB)
{
	MockDB mockDB;
	EXPECT_CALL(mockDB, UpdateSolvedUserStories(testing::_))
	.Times(1)
	.WillOnce(testing::Return(true));

	sprint->SetDB(&mockDB);
	sprint->AddUserStories(7);
	EXPECT_TRUE(sprint->UpdateDB());
}
