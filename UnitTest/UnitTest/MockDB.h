#pragma once

class IDataBase 
{
public :
	virtual ~IDataBase() {};
	virtual bool UpdateSolvedUserStories(int num) = 0;
};

class MockDB : public IDataBase
{
public:
	MockDB() {};
	virtual ~MockDB() {};
	MOCK_METHOD1(UpdateSolvedUserStories, bool(int num));
};